on run {input, parameters}
	tell application "Finder"
		set dir_path to quoted form of (POSIX path of (folder of the front window as alias))
	end tell
	CD_to(dir_path)
end run

on CD_to(dir_path)
	# todo: detect if iTerm is installed or not
	# todo: check support for iTerm vs iTerm2
	try
		tell application "iTerm"
			create window with default profile
			tell current session of current window
				write text "cd " & dir_path & ";clear;"
			end tell
		end tell
	on error err_msg
		display dialog "Error: " & err_msg
	end try
end CD_to
