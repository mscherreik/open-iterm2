Open the current Finder window in a new iTerm2 window.

To install:

1. Open Automator
2. Create a new Application
3. Add "Run AppleScript" Action from the left sidebar
4. Paste the code from open_iterm.scpt
5. Save your application to some directory
6. Hold Cmd+Shift and drag the icon for your new application to the Finder toolbar


